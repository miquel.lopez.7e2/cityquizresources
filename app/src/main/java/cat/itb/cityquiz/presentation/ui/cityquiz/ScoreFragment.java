package cat.itb.cityquiz.presentation.ui.cityquiz;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;

public class ScoreFragment extends Fragment {

    private CityQuizViewModel mViewModel;


    TextView scorePoints;

    Button botoPlayAgain;

    public static ScoreFragment newInstance() {
        return new ScoreFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.score_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(CityQuizViewModel.class);
        mViewModel.getGame().observe(this,this::display);

        // TODO: Use the ViewModel
    }

    private void display(Game game) {
        scorePoints.setText(String.valueOf(game.getNumCorrectAnswers()));
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        botoPlayAgain = getView().findViewById(R.id.play_again_button);
        scorePoints = getView().findViewById(R.id.score_number);
        botoPlayAgain.setOnClickListener(this::playAgain);
    }

    public void playAgain(View view) {
        mViewModel.startQuiz();
        Navigation.findNavController(view).navigate(R.id.action_scoreFragment2_to_questionFragment2);
    }
}
