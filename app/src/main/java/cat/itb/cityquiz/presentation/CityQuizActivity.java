package cat.itb.cityquiz.presentation;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.ui.NavigationUI;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.presentation.ui.cityquiz.StartQuizFragment;

import static androidx.navigation.Navigation.findNavController;

public class CityQuizActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.city_quiz_activity);
       /* // Mostra la fletxa de retorn cap enrere
        NavigationUI.setupActionBarWithNavController(this,findNavController(this, R.id.nav_host_fragment));*/
    }
/*
    @Override
    public boolean onSupportNavigateUp() {

        return findNavController(this, R.id.nav_host_fragment).navigateUp();
    }*/


}
