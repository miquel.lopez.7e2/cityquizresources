package cat.itb.cityquiz.presentation.ui.cityquiz;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import java.util.Objects;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;

public class StartQuizFragment extends Fragment {

    private CityQuizViewModel mViewModel;

    Button botoStart;

    public static StartQuizFragment newInstance() {
        return new StartQuizFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.start_quiz_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(CityQuizViewModel.class);

        mViewModel.getGame().observe(this,this::onGameChanged);


        // TODO: Use the ViewModel
    }

    private void onGameChanged(Game game) {
        if(game!=null){
            Navigation.findNavController(Objects.requireNonNull(getView())).navigate(R.id.action_startQuizFragment_to_questionFragment2);
        }
    }

    // En aquest métode s'inicialitza el botó i es vincula amb el métode per a canviar de Fragment

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        botoStart = getView().findViewById(R.id.start_quiz_button);
        botoStart.setOnClickListener(this::startGame);


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void startGame(View view) {
        mViewModel.startQuiz();




    }

}
