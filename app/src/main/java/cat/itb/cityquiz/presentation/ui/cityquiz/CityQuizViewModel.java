package cat.itb.cityquiz.presentation.ui.cityquiz;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;



import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class CityQuizViewModel extends ViewModel {


    GameLogic gameLogic;
    MutableLiveData<Game> game = new MutableLiveData<>();

    public void startQuiz() {
        gameLogic = RepositoriesFactory.getGameLogic();
        Game gameLocal = gameLogic.createGame(Game.maxQuestions,Game.possibleAnswers);
        game.postValue(gameLocal);
    }

    public MutableLiveData<Game> getGame() {
        return game;
    }

    public void answerQuestion(int anwser) {
        game.postValue(gameLogic.answerQuestions(game.getValue(), anwser));

    }


}
