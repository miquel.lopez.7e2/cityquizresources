package cat.itb.cityquiz.presentation.ui.cityquiz;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import java.util.List;
import java.util.Objects;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.domain.Question;

public class QuestionFragment extends Fragment {

    private CityQuizViewModel mViewModel;
    private Button button1;
    private Button button2;
    private Button button3;
    private Button button4;
    private Button button5;
    private Button button6;


    private ImageView imageView;


    public static QuestionFragment newInstance() {
        return new QuestionFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.question_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(CityQuizViewModel.class);
        mViewModel.getGame().observe(this,this::display);


    }

    private void display(Game game) {

        //FOTO

        if (!game.isFinished()) {


        String fileName = ImagesDownloader.scapeName(game.getCurrentQuestion().getCorrectCity().getName());
        int resId = Objects.requireNonNull(getContext()).getResources().getIdentifier(fileName, "drawable", getContext().getPackageName());
        imageView.setImageResource(resId);

        //BOTONES

        Question question = game.getCurrentQuestion();

        button1.setText(String.valueOf(question.getPossibleCities().get(0).getName()));
        button2.setText(String.valueOf(question.getPossibleCities().get(1).getName()));
        button3.setText(String.valueOf(question.getPossibleCities().get(2).getName()));
        button4.setText(String.valueOf(question.getPossibleCities().get(3).getName()));
        button5.setText(String.valueOf(question.getPossibleCities().get(4).getName()));
        button6.setText(String.valueOf(question.getPossibleCities().get(5).getName()));
        }

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        imageView = Objects.requireNonNull(getView()).findViewById(R.id.questionImage);
        button1 = getView().findViewById(R.id.option1);
        button2 = getView().findViewById(R.id.option2);
        button3 = getView().findViewById(R.id.option3);
        button4 = getView().findViewById(R.id.option4);
        button5 = getView().findViewById(R.id.option5);
        button6 = getView().findViewById(R.id.option6);


        button1.setOnClickListener(this::buttonOptionClicked);
        button2.setOnClickListener(this::buttonOptionClicked);
        button3.setOnClickListener(this::buttonOptionClicked);
        button4.setOnClickListener(this::buttonOptionClicked);
        button5.setOnClickListener(this::buttonOptionClicked);
        button6.setOnClickListener(this::buttonOptionClicked);

    }

    public void buttonOptionClicked(View view) {

        if(view == button1){
            questionAnswered(0);
        } else if(view == button2) {
            questionAnswered(1);
        } else if (view == button3) {
            questionAnswered(2);
        } else if (view == button4) {
            questionAnswered(3);
        } else if (view == button5) {
            questionAnswered(4);
        } else if (view == button6) {
            questionAnswered(5);
        }

        display(Objects.requireNonNull(mViewModel.getGame().getValue()));

        if (mViewModel.getGame().getValue().isFinished()) {
            Navigation.findNavController(view).navigate(R.id.action_questionFragment2_to_scoreFragment2);
        }



    }

    private void questionAnswered(int i) {
        mViewModel.answerQuestion(i);
    }

}
